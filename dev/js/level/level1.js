/**
 * @Date:   2018-10-09T17:48:30+02:00
 * @Last modified time: 2018-10-15T14:50:47+02:00
 */



function firstlevel_init(){


  // initialize level 1

  console.log("This players are logged in:");
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    console.log(MTLG.getPlayer(i));
  }


  console.log("Thie are the available game options:");
  console.log(MTLG.getOptions());

  // esit the game.settings.js
  console.log("Thie are the available game settings:");
  console.log(MTLG.getSettings());

  drawLevel1();
}

// check wether level 1 is choosen or not
function checkLevel1(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel == "level1"){
    return 1;
  }
  return 0;
}


function drawLevel1(){


  var stage = MTLG.getStageContainer();

  // Erstellt blaues Rechteck in der linken oberen Ecke
  let bluerect = MTLG.utils.gfx.getShape();
  bluerect.graphics.beginFill('blue').drawRect(0,0,50,50);
  bluerect.x = 100;
  bluerect.y = 100;
  bluerect.on('pressmove', draggable);
  stage.addChild(bluerect);

  // Erstellt grünes Rechteck in der rechten, unteren Ecke
  let greenrect = MTLG.utils.gfx.getShape();
  greenrect.graphics.beginFill('green').drawRect(0,0,200,200);
  greenrect.x = 1500;
  greenrect.y = 800;
  stage.addChild(greenrect);

  // Erstellt rotes Rechteck in der rechten, oberen Ecke
  let redrect = MTLG.utils.gfx.getShape();
  redrect.graphics.beginFill('red').drawRect(0,0,200,200);
  redrect.x = 1500;
  redrect.y = 100;
  stage.addChild(redrect);


  function draggable(e) {
    let p = e.currentTarget.localToLocal(e.localX, e.localY, stage);
    e.currentTarget.set(p);         // Verstzt zu ziehendes Objekt auf die Position des Mauszeigers
    //Deckt den Kollisionsbereich um das rote Rechteck ab
    if(bluerect.y >= redrect.y - 50 && bluerect.y <= redrect.y + 200 && bluerect.x >= redrect.x - 50 && bluerect.x <= redrect.x + 200){
      alert("Hier könnte ihre Erfolgsmeldung stehen");
      MTLG.lc.goToMenu();
    }
    else if (bluerect.y >= greenrect.y - 50 && bluerect.y <= greenrect.y + 200 && bluerect.x >= greenrect.x - 50 && bluerect.x <= greenrect.x + 200) {
      MTLG.lc.levelFinished({       //sorgt dafür, dass ein nächster Level angefordert wird (also gameState.nextLevel TRUE wird)
        nextLevel: "level1",        //übergebe als gameState.nextLevel level1
      });
    }

  }

  console.log("Level 1 started.");

  // add objects to stage ####################################################


}
